<?php

namespace App\Http\Controllers\Api;

use App\Adv;
use App\Blog;
use App\Brand;
use App\Category;
use App\CategorySlider;
use App\Commission;
use App\CommissionSetting;
use App\Faq;
use App\FooterMenu;
use App\Genral;
use App\Grandcategory;
use App\Hotdeal;
use App\Http\Controllers\Controller;
use App\Menu;
use App\Page;
use App\Product;
use App\ProductAttributes;
use App\ProductValues;
use App\Slider;
use App\SpecialOffer;
use App\Subcategory;
use App\Testimonial;
use App\UserReview;
use App\UserWallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Validator;

/*==========================================
=            emart Rest APIs               =
=            Author: Media City            =
Author URI: https://mediacity.co.in
=            Developer : @nkit             =
=            Copyright (c) 2020            =
==========================================*/

class MainController extends Controller
{
    public function __construct()
    {
        require_once base_path() . '/app/Http/Controllers/price.php';
        $this->conversion_rate = $conversion_rate;
    }

    public function homepage(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'secret' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['Secret Key is required']);
        }

        $key = DB::table('api_keys')->where('secret_key', '=', $request->secret)->first();

        if (!$key) {
            return response()->json(['Invalid Secret Key !']);
        }

        $item = array();

        $content = array();

        /** List app settings */
        $response = $this->appSettings();

        $response = $response->getData();

        $item[] = array(
            'name' => 'appheader',
            'layout' => 'appheader',
            'logopath' => $response->logopath,
            'logo' => $response->logo,
        );
        /** End */

        /** If Above Slider there is some Adovertise */

        $threeimg = Adv::where('position', '=', 'beforeslider')->where('layout', 'Three Image Layout')->where('status', '=', 1)->get();

        if (!empty($threeimg)) {
            foreach ($threeimg as $key => $ads) {

                $images = array();

                if ($ads->cat_id1 != '') {
                    $image1Linking = 'category';
                    $linkedID1 = $ads->cat_id1;
                } elseif ($ads->url1 != '') {
                    $image1Linking = 'url';
                    $linkedID1 = $ads->url1;
                } elseif ($ads->pro_id1 != '') {
                    $image1Linking = 'product';
                    $linkedID1 = $ads->pro_id1;
                }

                if ($ads->cat_id2 != '') {
                    $image2Linking = 'category';
                    $linkedID2 = $ads->cat_id2;
                } elseif ($ads->url2 != '') {
                    $image2Linking = 'url';
                    $linkedID2 = $ads->url2;
                } elseif ($ads->pro_id2 != '') {
                    $image2Linking = 'product';
                    $linkedID3 = $ads->pro_id2;
                }

                if ($ads->cat_id3 != '') {
                    $image3Linking = 'category';
                    $linkedID3 = $ads->cat_id3;
                } elseif ($ads->url3 != '') {
                    $image3Linking = 'url';
                    $linkedID3 = $ads->url3;
                } elseif ($ads->pro_id3 != '') {
                    $image3Linking = 'product';
                    $linkedID3 = $ads->pro_id3;
                }

                $images[] = array(
                    'size' => 'M',
                    'linkedto' => $image1Linking,
                    'linkedid' => $linkedID1,
                    'image' => $ads->image1,
                );

                $images[] = array(
                    'size' => 'M',
                    'linkedto' => $image2Linking,
                    'linkedid' => $linkedID2,
                    'image' => $ads->image2,
                );

                $images[] = array(
                    'size' => 'M',
                    'linkedto' => $image3Linking,
                    'linkedid' => $linkedID3,
                    'image' => $ads->image3,
                );

                $item[] = array(
                    'layout' => 'advertisements',
                    'name' => $ads->layout,
                    'enable' => true,
                    'path' => url('/images/adv/'),
                    'items' => $images,
                );
            }
        }

        $twong = Adv::where('position', '=', 'beforeslider')->where('layout', 'Two non equal image layout')->where('status', '=', 1)->get();

        if (!empty($twong)) {
            foreach ($twong as $key => $ads) {

                $images = array();

                if ($ads->cat_id1 != '') {
                    $image1Linking = 'category';
                    $linkedID1 = $ads->cat_id1;
                } elseif ($ads->url1 != '') {
                    $image1Linking = 'url';
                    $linkedID1 = $ads->url1;
                } elseif ($ads->pro_id1 != '') {
                    $image1Linking = 'product';
                    $linkedID1 = $ads->pro_id1;
                }

                if ($ads->cat_id2 != '') {
                    $image2Linking = 'category';
                    $linkedID2 = $ads->cat_id2;
                } elseif ($ads->url2 != '') {
                    $image2Linking = 'url';
                    $linkedID2 = $ads->url2;
                } elseif ($ads->pro_id2 != '') {
                    $image2Linking = 'product';
                    $linkedID3 = $ads->pro_id2;
                }

                $images[] = array(
                    'size' => 'L',
                    'linkedto' => $image1Linking,
                    'linkedid' => $linkedID1,
                    'image' => $ads->image1,
                );

                $images[] = array(
                    'size' => 'S',
                    'linkedto' => $image2Linking,
                    'linkedid' => $linkedID2,
                    'image' => $ads->image2,
                );

                $item[] = array(
                    'layout' => 'advertisements',
                    'name' => $ads->layout,
                    'enable' => true,
                    'path' => url('/images/adv/'),
                    'items' => $images,
                );
            }
        }

        $twoeq = Adv::where('position', '=', 'beforeslider')->where('layout', 'Two equal image layout')->where('status', '=', 1)->get();

        if (!empty($twoeq)) {
            foreach ($twoeq as $key => $ads) {

                $images = array();

                if ($ads->cat_id1 != '') {
                    $image1Linking = 'category';
                    $linkedID1 = $ads->cat_id1;
                } elseif ($ads->url1 != '') {
                    $image1Linking = 'url';
                    $linkedID1 = $ads->url1;
                } elseif ($ads->pro_id1 != '') {
                    $image1Linking = 'product';
                    $linkedID1 = $ads->pro_id1;
                }

                if ($ads->cat_id2 != '') {
                    $image2Linking = 'category';
                    $linkedID2 = $ads->cat_id2;
                } elseif ($ads->url2 != '') {
                    $image2Linking = 'url';
                    $linkedID2 = $ads->url2;
                } elseif ($ads->pro_id2 != '') {
                    $image2Linking = 'product';
                    $linkedID3 = $ads->pro_id2;
                }

                $images[] = array(
                    'size' => 'M',
                    'linkedto' => $image1Linking,
                    'linkedid' => $linkedID1,
                    'image' => $ads->image1,
                );

                $images[] = array(
                    'size' => 'M',
                    'linkedto' => $image2Linking,
                    'linkedid' => $linkedID2,
                    'image' => $ads->image2,
                );

                $item[] = array(
                    'layout' => 'advertisements',
                    'name' => $ads->layout,
                    'enable' => true,
                    'path' => url('/images/adv/'),
                    'items' => $images,
                );
            }
        }

        $singleimg = Adv::where('position', '=', 'beforeslider')->where('layout', 'Single image layout')->where('status', '=', 1)->get();

        if (!empty($singleimg)) {
            foreach ($singleimg as $key => $ads) {

                $images = array();

                if ($ads->cat_id1 != '') {
                    $image1Linking = 'category';
                    $linkedID1 = $ads->cat_id1;
                } elseif ($ads->url1 != '') {
                    $image1Linking = 'url';
                    $linkedID1 = $ads->url1;
                } elseif ($ads->pro_id1 != '') {
                    $image1Linking = 'product';
                    $linkedID1 = $ads->pro_id1;
                }

                $images[] = array(
                    'size' => 'XL',
                    'linkedto' => $image1Linking,
                    'linkedid' => $linkedID1,
                    'image' => $ads->image1,
                );

                $item[] = array(
                    'layout' => 'advertisements',
                    'name' => $ads->layout,
                    'enable' => true,
                    'path' => url('/images/adv/'),
                    'items' => $images,
                );
            }
        }

        /** Getting Sliders */

        $response = $this->slider($content);

        /** End */

        $item[] = array(
            'name' => 'slider',
            'layout' => 'sliders',
            'autoslide' => true,
            'enable' => true,
            'path' => url('images/slider'),
            'items' => $response,
        );

        /** If below Slider there is some Adovertise */

        $threeimg = Adv::where('position', '=', 'abovenewproduct')->where('layout', 'Three Image Layout')->where('status', '=', 1)->get();

        if (!empty($threeimg)) {
            foreach ($threeimg as $key => $ads) {

                $images = array();

                if ($ads->cat_id1 != '') {
                    $image1Linking = 'category';
                    $linkedID1 = $ads->cat_id1;
                } elseif ($ads->url1 != '') {
                    $image1Linking = 'url';
                    $linkedID1 = $ads->url1;
                } elseif ($ads->pro_id1 != '') {
                    $image1Linking = 'product';
                    $linkedID1 = $ads->pro_id1;
                }

                if ($ads->cat_id2 != '') {
                    $image2Linking = 'category';
                    $linkedID2 = $ads->cat_id2;
                } elseif ($ads->url2 != '') {
                    $image2Linking = 'url';
                    $linkedID2 = $ads->url2;
                } elseif ($ads->pro_id2 != '') {
                    $image2Linking = 'product';
                    $linkedID3 = $ads->pro_id2;
                }

                if ($ads->cat_id3 != '') {
                    $image3Linking = 'category';
                    $linkedID3 = $ads->cat_id3;
                } elseif ($ads->url3 != '') {
                    $image3Linking = 'url';
                    $linkedID3 = $ads->url3;
                } elseif ($ads->pro_id3 != '') {
                    $image3Linking = 'product';
                    $linkedID3 = $ads->pro_id3;
                }

                $images[] = array(
                    'size' => 'M',
                    'linkedto' => $image1Linking,
                    'linkedid' => $linkedID1,
                    'image' => $ads->image1,
                );

                $images[] = array(
                    'size' => 'M',
                    'linkedto' => $image2Linking,
                    'linkedid' => $linkedID2,
                    'image' => $ads->image2,
                );

                $images[] = array(
                    'size' => 'M',
                    'linkedto' => $image3Linking,
                    'linkedid' => $linkedID3,
                    'image' => $ads->image3,
                );

                $item[] = array(
                    'layout' => 'advertisements',
                    'name' => $ads->layout,
                    'enable' => true,
                    'path' => url('/images/adv/'),
                    'items' => $images,
                );
            }
        }

        $twong = Adv::where('position', '=', 'abovenewproduct')->where('layout', 'Two non equal image layout')->where('status', '=', 1)->get();

        if (!empty($twong)) {
            foreach ($twong as $key => $ads) {

                $images = array();

                if ($ads->cat_id1 != '') {
                    $image1Linking = 'category';
                    $linkedID1 = $ads->cat_id1;
                } elseif ($ads->url1 != '') {
                    $image1Linking = 'url';
                    $linkedID1 = $ads->url1;
                } elseif ($ads->pro_id1 != '') {
                    $image1Linking = 'product';
                    $linkedID1 = $ads->pro_id1;
                }

                if ($ads->cat_id2 != '') {
                    $image2Linking = 'category';
                    $linkedID2 = $ads->cat_id2;
                } elseif ($ads->url2 != '') {
                    $image2Linking = 'url';
                    $linkedID2 = $ads->url2;
                } elseif ($ads->pro_id2 != '') {
                    $image2Linking = 'product';
                    $linkedID3 = $ads->pro_id2;
                }

                $images[] = array(
                    'size' => 'L',
                    'linkedto' => $image1Linking,
                    'linkedid' => $linkedID1,
                    'image' => $ads->image1,
                );

                $images[] = array(
                    'size' => 'S',
                    'linkedto' => $image2Linking,
                    'linkedid' => $linkedID2,
                    'image' => $ads->image2,
                );

                $item[] = array(
                    'layout' => 'advertisements',
                    'name' => $ads->layout,
                    'enable' => true,
                    'path' => url('/images/adv/'),
                    'items' => $images,
                );
            }
        }

        $twoeq = Adv::where('position', '=', 'abovenewproduct')->where('layout', 'Two equal image layout')->where('status', '=', 1)->get();

        if (!empty($twoeq)) {
            foreach ($twoeq as $key => $ads) {

                $images = array();

                if ($ads->cat_id1 != '') {
                    $image1Linking = 'category';
                    $linkedID1 = $ads->cat_id1;
                } elseif ($ads->url1 != '') {
                    $image1Linking = 'url';
                    $linkedID1 = $ads->url1;
                } elseif ($ads->pro_id1 != '') {
                    $image1Linking = 'product';
                    $linkedID1 = $ads->pro_id1;
                }

                if ($ads->cat_id2 != '') {
                    $image2Linking = 'category';
                    $linkedID2 = $ads->cat_id2;
                } elseif ($ads->url2 != '') {
                    $image2Linking = 'url';
                    $linkedID2 = $ads->url2;
                } elseif ($ads->pro_id2 != '') {
                    $image2Linking = 'product';
                    $linkedID3 = $ads->pro_id2;
                }

                $images[] = array(
                    'size' => 'M',
                    'linkedto' => $image1Linking,
                    'linkedid' => $linkedID1,
                    'image' => $ads->image1,
                );

                $images[] = array(
                    'size' => 'M',
                    'linkedto' => $image2Linking,
                    'linkedid' => $linkedID2,
                    'image' => $ads->image2,
                );

                $item[] = array(
                    'layout' => 'advertisements',
                    'name' => $ads->layout,
                    'enable' => true,
                    'path' => url('/images/adv/'),
                    'items' => $images,
                );
            }
        }

        $singleimg = Adv::where('position', '=', 'abovenewproduct')->where('layout', 'Single image layout')->where('status', '=', 1)->get();

        if (!empty($singleimg)) {
            foreach ($singleimg as $key => $ads) {

                $images = array();

                if ($ads->cat_id1 != '') {
                    $image1Linking = 'category';
                    $linkedID1 = $ads->cat_id1;
                } elseif ($ads->url1 != '') {
                    $image1Linking = 'url';
                    $linkedID1 = $ads->url1;
                } elseif ($ads->pro_id1 != '') {
                    $image1Linking = 'product';
                    $linkedID1 = $ads->pro_id1;
                }

                $images[] = array(
                    'size' => 'XL',
                    'linkedto' => $image1Linking,
                    'linkedid' => $linkedID1,
                    'image' => $ads->image1,
                );

                $item[] = array(
                    'layout' => 'advertisements',
                    'name' => $ads->layout,
                    'enable' => true,
                    'path' => url('/images/adv/'),
                    'items' => $images,
                );
            }
        }

        /** end */

        /** Recent Products */

        $response = $this->recentProducts($content);

        $item[] = array(
            'scrolltype' => 'vertical',
            'name' => 'recentproducts',
            'layout' => 'productblock',
            'enable' => true,
            'path' => url('variantimages/thumbnails/'),
            'items' => $response,
        );

        /** If below above blogs there is some Adovertise */

        $threeimg = Adv::where('position', '=', 'abovetopcategory')->where('layout', 'Three Image Layout')->where('status', '=', 1)->get();

        if (!empty($threeimg)) {
            foreach ($threeimg as $key => $ads) {

                $images = array();

                if ($ads->cat_id1 != '') {
                    $image1Linking = 'category';
                    $linkedID1 = $ads->cat_id1;
                } elseif ($ads->url1 != '') {
                    $image1Linking = 'url';
                    $linkedID1 = $ads->url1;
                } elseif ($ads->pro_id1 != '') {
                    $image1Linking = 'product';
                    $linkedID1 = $ads->pro_id1;
                }

                if ($ads->cat_id2 != '') {
                    $image2Linking = 'category';
                    $linkedID2 = $ads->cat_id2;
                } elseif ($ads->url2 != '') {
                    $image2Linking = 'url';
                    $linkedID2 = $ads->url2;
                } elseif ($ads->pro_id2 != '') {
                    $image2Linking = 'product';
                    $linkedID3 = $ads->pro_id2;
                }

                if ($ads->cat_id3 != '') {
                    $image3Linking = 'category';
                    $linkedID3 = $ads->cat_id3;
                } elseif ($ads->url3 != '') {
                    $image3Linking = 'url';
                    $linkedID3 = $ads->url3;
                } elseif ($ads->pro_id3 != '') {
                    $image3Linking = 'product';
                    $linkedID3 = $ads->pro_id3;
                }

                $images[] = array(
                    'size' => 'M',
                    'linkedto' => $image1Linking,
                    'linkedid' => $linkedID1,
                    'image' => $ads->image1,
                );

                $images[] = array(
                    'size' => 'M',
                    'linkedto' => $image2Linking,
                    'linkedid' => $linkedID2,
                    'image' => $ads->image2,
                );

                $images[] = array(
                    'size' => 'M',
                    'linkedto' => $image3Linking,
                    'linkedid' => $linkedID3,
                    'image' => $ads->image3,
                );

                $item[] = array(
                    'layout' => 'advertisements',
                    'name' => $ads->layout,
                    'enable' => true,
                    'path' => url('/images/adv/'),
                    'items' => $images,
                );
            }
        }

        $twong = Adv::where('position', '=', 'abovetopcategory')->where('layout', 'Two non equal image layout')->where('status', '=', 1)->get();

        if (!empty($twong)) {
            foreach ($twong as $key => $ads) {

                $images = array();

                if ($ads->cat_id1 != '') {
                    $image1Linking = 'category';
                    $linkedID1 = $ads->cat_id1;
                } elseif ($ads->url1 != '') {
                    $image1Linking = 'url';
                    $linkedID1 = $ads->url1;
                } elseif ($ads->pro_id1 != '') {
                    $image1Linking = 'product';
                    $linkedID1 = $ads->pro_id1;
                }

                if ($ads->cat_id2 != '') {
                    $image2Linking = 'category';
                    $linkedID2 = $ads->cat_id2;
                } elseif ($ads->url2 != '') {
                    $image2Linking = 'url';
                    $linkedID2 = $ads->url2;
                } elseif ($ads->pro_id2 != '') {
                    $image2Linking = 'product';
                    $linkedID3 = $ads->pro_id2;
                }

                $images[] = array(
                    'size' => 'L',
                    'linkedto' => $image1Linking,
                    'linkedid' => $linkedID1,
                    'image' => $ads->image1,
                );

                $images[] = array(
                    'size' => 'S',
                    'linkedto' => $image2Linking,
                    'linkedid' => $linkedID2,
                    'image' => $ads->image2,
                );

                $item[] = array(
                    'layout' => 'advertisements',
                    'name' => $ads->layout,
                    'enable' => true,
                    'path' => url('/images/adv/'),
                    'items' => $images,
                );
            }
        }

        $twoeq = Adv::where('position', '=', 'abovetopcategory')->where('layout', 'Two equal image layout')->where('status', '=', 1)->get();

        if (!empty($twoeq)) {
            foreach ($twoeq as $key => $ads) {

                $images = array();

                if ($ads->cat_id1 != '') {
                    $image1Linking = 'category';
                    $linkedID1 = $ads->cat_id1;
                } elseif ($ads->url1 != '') {
                    $image1Linking = 'url';
                    $linkedID1 = $ads->url1;
                } elseif ($ads->pro_id1 != '') {
                    $image1Linking = 'product';
                    $linkedID1 = $ads->pro_id1;
                }

                if ($ads->cat_id2 != '') {
                    $image2Linking = 'category';
                    $linkedID2 = $ads->cat_id2;
                } elseif ($ads->url2 != '') {
                    $image2Linking = 'url';
                    $linkedID2 = $ads->url2;
                } elseif ($ads->pro_id2 != '') {
                    $image2Linking = 'product';
                    $linkedID3 = $ads->pro_id2;
                }

                $images[] = array(
                    'size' => 'M',
                    'linkedto' => $image1Linking,
                    'linkedid' => $linkedID1,
                    'image' => $ads->image1,
                );

                $images[] = array(
                    'size' => 'M',
                    'linkedto' => $image2Linking,
                    'linkedid' => $linkedID2,
                    'image' => $ads->image2,
                );

                $item[] = array(
                    'layout' => 'advertisements',
                    'name' => $ads->layout,
                    'enable' => true,
                    'path' => url('/images/adv/'),
                    'items' => $images,
                );
            }
        }

        $singleimg = Adv::where('position', '=', 'abovetopcategory')->where('layout', 'Single image layout')->where('status', '=', 1)->get();

        if (!empty($singleimg)) {
            foreach ($singleimg as $key => $ads) {

                $images = array();

                if ($ads->cat_id1 != '') {
                    $image1Linking = 'category';
                    $linkedID1 = $ads->cat_id1;
                } elseif ($ads->url1 != '') {
                    $image1Linking = 'url';
                    $linkedID1 = $ads->url1;
                } elseif ($ads->pro_id1 != '') {
                    $image1Linking = 'product';
                    $linkedID1 = $ads->pro_id1;
                }

                $images[] = array(
                    'size' => 'XL',
                    'linkedto' => $image1Linking,
                    'linkedid' => $linkedID1,
                    'image' => $ads->image1,
                );

                $item[] = array(
                    'layout' => 'advertisements',
                    'name' => $ads->layout,
                    'enable' => true,
                    'path' => url('/images/adv/'),
                    'items' => $images,
                );
            }
        }

        /** End */

        /** Top categories */

        $response = $this->topcategories($content);

        $item[] = array(
            'scrolltype' => 'vertical',
            'name' => 'topcategories',
            'layout' => 'categoryblock',
            'enable' => true,
            'path' => url('/images/category/'),
            'items' => $response,
        );

        /** End */

        /** If below above blogs there is some Adovertise */

        $threeimg = Adv::where('position', '=', 'abovelatestblog')->where('layout', 'Three Image Layout')->where('status', '=', 1)->get();

        if (!empty($threeimg)) {
            foreach ($threeimg as $key => $ads) {

                $images = array();

                if ($ads->cat_id1 != '') {
                    $image1Linking = 'category';
                    $linkedID1 = $ads->cat_id1;
                } elseif ($ads->url1 != '') {
                    $image1Linking = 'url';
                    $linkedID1 = $ads->url1;
                } elseif ($ads->pro_id1 != '') {
                    $image1Linking = 'product';
                    $linkedID1 = $ads->pro_id1;
                }

                if ($ads->cat_id2 != '') {
                    $image2Linking = 'category';
                    $linkedID2 = $ads->cat_id2;
                } elseif ($ads->url2 != '') {
                    $image2Linking = 'url';
                    $linkedID2 = $ads->url2;
                } elseif ($ads->pro_id2 != '') {
                    $image2Linking = 'product';
                    $linkedID3 = $ads->pro_id2;
                }

                if ($ads->cat_id3 != '') {
                    $image3Linking = 'category';
                    $linkedID3 = $ads->cat_id3;
                } elseif ($ads->url3 != '') {
                    $image3Linking = 'url';
                    $linkedID3 = $ads->url3;
                } elseif ($ads->pro_id3 != '') {
                    $image3Linking = 'product';
                    $linkedID3 = $ads->pro_id3;
                }

                $images[] = array(
                    'size' => 'M',
                    'linkedto' => $image1Linking,
                    'linkedid' => $linkedID1,
                    'image' => $ads->image1,
                );

                $images[] = array(
                    'size' => 'M',
                    'linkedto' => $image2Linking,
                    'linkedid' => $linkedID2,
                    'image' => $ads->image2,
                );

                $images[] = array(
                    'size' => 'M',
                    'linkedto' => $image3Linking,
                    'linkedid' => $linkedID3,
                    'image' => $ads->image3,
                );

                $item[] = array(
                    'layout' => 'advertisements',
                    'name' => $ads->layout,
                    'enable' => true,
                    'path' => url('/images/adv/'),
                    'items' => $images,
                );
            }
        }

        $twong = Adv::where('position', '=', 'abovelatestblog')->where('layout', 'Two non equal image layout')->where('status', '=', 1)->get();

        if (!empty($twong)) {
            foreach ($twong as $key => $ads) {

                $images = array();

                if ($ads->cat_id1 != '') {
                    $image1Linking = 'category';
                    $linkedID1 = $ads->cat_id1;
                } elseif ($ads->url1 != '') {
                    $image1Linking = 'url';
                    $linkedID1 = $ads->url1;
                } elseif ($ads->pro_id1 != '') {
                    $image1Linking = 'product';
                    $linkedID1 = $ads->pro_id1;
                }

                if ($ads->cat_id2 != '') {
                    $image2Linking = 'category';
                    $linkedID2 = $ads->cat_id2;
                } elseif ($ads->url2 != '') {
                    $image2Linking = 'url';
                    $linkedID2 = $ads->url2;
                } elseif ($ads->pro_id2 != '') {
                    $image2Linking = 'product';
                    $linkedID3 = $ads->pro_id2;
                }

                $images[] = array(
                    'size' => 'L',
                    'linkedto' => $image1Linking,
                    'linkedid' => $linkedID1,
                    'image' => $ads->image1,
                );

                $images[] = array(
                    'size' => 'S',
                    'linkedto' => $image2Linking,
                    'linkedid' => $linkedID2,
                    'image' => $ads->image2,
                );

                $item[] = array(
                    'layout' => 'advertisements',
                    'name' => $ads->layout,
                    'enable' => true,
                    'path' => url('/images/adv/'),
                    'items' => $images,
                );
            }
        }

        $twoeq = Adv::where('position', '=', 'abovelatestblog')->where('layout', 'Two equal image layout')->where('status', '=', 1)->get();

        if (!empty($twoeq)) {
            foreach ($twoeq as $key => $ads) {

                $images = array();

                if ($ads->cat_id1 != '') {
                    $image1Linking = 'category';
                    $linkedID1 = $ads->cat_id1;
                } elseif ($ads->url1 != '') {
                    $image1Linking = 'url';
                    $linkedID1 = $ads->url1;
                } elseif ($ads->pro_id1 != '') {
                    $image1Linking = 'product';
                    $linkedID1 = $ads->pro_id1;
                }

                if ($ads->cat_id2 != '') {
                    $image2Linking = 'category';
                    $linkedID2 = $ads->cat_id2;
                } elseif ($ads->url2 != '') {
                    $image2Linking = 'url';
                    $linkedID2 = $ads->url2;
                } elseif ($ads->pro_id2 != '') {
                    $image2Linking = 'product';
                    $linkedID3 = $ads->pro_id2;
                }

                $images[] = array(
                    'size' => 'M',
                    'linkedto' => $image1Linking,
                    'linkedid' => $linkedID1,
                    'image' => $ads->image1,
                );

                $images[] = array(
                    'size' => 'M',
                    'linkedto' => $image2Linking,
                    'linkedid' => $linkedID2,
                    'image' => $ads->image2,
                );

                $item[] = array(
                    'layout' => 'advertisements',
                    'name' => $ads->layout,
                    'enable' => true,
                    'path' => url('/images/adv/'),
                    'items' => $images,
                );
            }
        }

        $singleimg = Adv::where('position', '=', 'abovelatestblog')->where('layout', 'Single image layout')->where('status', '=', 1)->get();

        if (!empty($singleimg)) {
            foreach ($singleimg as $key => $ads) {

                $images = array();

                if ($ads->cat_id1 != '') {
                    $image1Linking = 'category';
                    $linkedID1 = $ads->cat_id1;
                } elseif ($ads->url1 != '') {
                    $image1Linking = 'url';
                    $linkedID1 = $ads->url1;
                } elseif ($ads->pro_id1 != '') {
                    $image1Linking = 'product';
                    $linkedID1 = $ads->pro_id1;
                }

                $images[] = array(
                    'size' => 'XL',
                    'linkedto' => $image1Linking,
                    'linkedid' => $linkedID1,
                    'image' => $ads->image1,
                );

                $item[] = array(
                    'layout' => 'advertisements',
                    'name' => $ads->layout,
                    'enable' => true,
                    'path' => url('/images/adv/'),
                    'items' => $images,
                );
            }
        }

        /** End */

        /** Getting Blogs */

        $response = $this->gettingBlogs($content);

        $item[] = array(
            'scrolltype' => 'vertical',
            'name' => 'blogs',
            'layout' => 'blog',
            'enable' => true,
            'path' => url('/images/blog/'),
            'items' => $response,
        );

        /** End */

        /** If above featured products there is some Adovertise */

        $threeimg = Adv::where('position', '=', 'abovefeaturedproduct')->where('layout', 'Three Image Layout')->where('status', '=', 1)->get();

        if (!empty($threeimg)) {
            foreach ($threeimg as $key => $ads) {

                $images = array();

                if ($ads->cat_id1 != '') {
                    $image1Linking = 'category';
                    $linkedID1 = $ads->cat_id1;
                } elseif ($ads->url1 != '') {
                    $image1Linking = 'url';
                    $linkedID1 = $ads->url1;
                } elseif ($ads->pro_id1 != '') {
                    $image1Linking = 'product';
                    $linkedID1 = $ads->pro_id1;
                }

                if ($ads->cat_id2 != '') {
                    $image2Linking = 'category';
                    $linkedID2 = $ads->cat_id2;
                } elseif ($ads->url2 != '') {
                    $image2Linking = 'url';
                    $linkedID2 = $ads->url2;
                } elseif ($ads->pro_id2 != '') {
                    $image2Linking = 'product';
                    $linkedID3 = $ads->pro_id2;
                }

                if ($ads->cat_id3 != '') {
                    $image3Linking = 'category';
                    $linkedID3 = $ads->cat_id3;
                } elseif ($ads->url3 != '') {
                    $image3Linking = 'url';
                    $linkedID3 = $ads->url3;
                } elseif ($ads->pro_id3 != '') {
                    $image3Linking = 'product';
                    $linkedID3 = $ads->pro_id3;
                }

                $images[] = array(
                    'size' => 'M',
                    'linkedto' => $image1Linking,
                    'linkedid' => $linkedID1,
                    'image' => $ads->image1,
                );

                $images[] = array(
                    'size' => 'M',
                    'linkedto' => $image2Linking,
                    'linkedid' => $linkedID2,
                    'image' => $ads->image2,
                );

                $images[] = array(
                    'size' => 'M',
                    'linkedto' => $image3Linking,
                    'linkedid' => $linkedID3,
                    'image' => $ads->image3,
                );

                $item[] = array(
                    'layout' => 'advertisements',
                    'name' => $ads->layout,
                    'enable' => true,
                    'path' => url('/images/adv/'),
                    'items' => $images,
                );
            }
        }

        $twong = Adv::where('position', '=', 'abovefeaturedproduct')->where('layout', 'Two non equal image layout')->where('status', '=', 1)->get();

        if (!empty($twong)) {
            foreach ($twong as $key => $ads) {

                $images = array();

                if ($ads->cat_id1 != '') {
                    $image1Linking = 'category';
                    $linkedID1 = $ads->cat_id1;
                } elseif ($ads->url1 != '') {
                    $image1Linking = 'url';
                    $linkedID1 = $ads->url1;
                } elseif ($ads->pro_id1 != '') {
                    $image1Linking = 'product';
                    $linkedID1 = $ads->pro_id1;
                }

                if ($ads->cat_id2 != '') {
                    $image2Linking = 'category';
                    $linkedID2 = $ads->cat_id2;
                } elseif ($ads->url2 != '') {
                    $image2Linking = 'url';
                    $linkedID2 = $ads->url2;
                } elseif ($ads->pro_id2 != '') {
                    $image2Linking = 'product';
                    $linkedID3 = $ads->pro_id2;
                }

                $images[] = array(
                    'size' => 'L',
                    'linkedto' => $image1Linking,
                    'linkedid' => $linkedID1,
                    'image' => $ads->image1,
                );

                $images[] = array(
                    'size' => 'S',
                    'linkedto' => $image2Linking,
                    'linkedid' => $linkedID2,
                    'image' => $ads->image2,
                );

                $item[] = array(
                    'layout' => 'advertisements',
                    'name' => $ads->layout,
                    'enable' => true,
                    'path' => url('/images/adv/'),
                    'items' => $images,
                );
            }
        }

        $twoeq = Adv::where('position', '=', 'abovefeaturedproduct')->where('layout', 'Two equal image layout')->where('status', '=', 1)->get();

        if (!empty($twoeq)) {
            foreach ($twoeq as $key => $ads) {

                $images = array();

                if ($ads->cat_id1 != '') {
                    $image1Linking = 'category';
                    $linkedID1 = $ads->cat_id1;
                } elseif ($ads->url1 != '') {
                    $image1Linking = 'url';
                    $linkedID1 = $ads->url1;
                } elseif ($ads->pro_id1 != '') {
                    $image1Linking = 'product';
                    $linkedID1 = $ads->pro_id1;
                }

                if ($ads->cat_id2 != '') {
                    $image2Linking = 'category';
                    $linkedID2 = $ads->cat_id2;
                } elseif ($ads->url2 != '') {
                    $image2Linking = 'url';
                    $linkedID2 = $ads->url2;
                } elseif ($ads->pro_id2 != '') {
                    $image2Linking = 'product';
                    $linkedID3 = $ads->pro_id2;
                }

                $images[] = array(
                    'size' => 'M',
                    'linkedto' => $image1Linking,
                    'linkedid' => $linkedID1,
                    'image' => $ads->image1,
                );

                $images[] = array(
                    'size' => 'M',
                    'linkedto' => $image2Linking,
                    'linkedid' => $linkedID2,
                    'image' => $ads->image2,
                );

                $item[] = array(
                    'layout' => 'advertisements',
                    'name' => $ads->layout,
                    'enable' => true,
                    'path' => url('/images/adv/'),
                    'items' => $images,
                );
            }
        }

        $singleimg = Adv::where('position', '=', 'abovefeaturedproduct')->where('layout', 'Single image layout')->where('status', '=', 1)->get();

        if (!empty($singleimg)) {
            foreach ($singleimg as $key => $ads) {

                $images = array();

                if ($ads->cat_id1 != '') {
                    $image1Linking = 'category';
                    $linkedID1 = $ads->cat_id1;
                } elseif ($ads->url1 != '') {
                    $image1Linking = 'url';
                    $linkedID1 = $ads->url1;
                } elseif ($ads->pro_id1 != '') {
                    $image1Linking = 'product';
                    $linkedID1 = $ads->pro_id1;
                }

                $images[] = array(
                    'size' => 'XL',
                    'linkedto' => $image1Linking,
                    'linkedid' => $linkedID1,
                    'image' => $ads->image1,
                );

                $item[] = array(
                    'layout' => ' cxments',
                    'name' => $ads->layout,
                    'enable' => true,
                    'path' => url('/images/adv/'),
                    'items' => $images,
                );
            }
        }
        /** End */

        /** Getting Featured Products */

        $response = $this->featuredProducts($content);

        $item[] = array(
            'scrolltype' => 'vertical',
            'name' => 'featuredproducts',
            'layout' => 'productblock',
            'enable' => true,
            'path' => url('variantimages/thumbnails/'),
            'items' => $response,
        );

        /** End */

        /** If above featured products there is some Adovertise */

        $threeimg = Adv::where('position', '=', 'afterfeaturedproduct')->where('layout', 'Three Image Layout')->where('status', '=', 1)->get();

        if (!empty($threeimg)) {
            foreach ($threeimg as $key => $ads) {

                $images = array();

                if ($ads->cat_id1 != '') {
                    $image1Linking = 'category';
                    $linkedID1 = $ads->cat_id1;
                } elseif ($ads->url1 != '') {
                    $image1Linking = 'url';
                    $linkedID1 = $ads->url1;
                } elseif ($ads->pro_id1 != '') {
                    $image1Linking = 'product';
                    $linkedID1 = $ads->pro_id1;
                }

                if ($ads->cat_id2 != '') {
                    $image2Linking = 'category';
                    $linkedID2 = $ads->cat_id2;
                } elseif ($ads->url2 != '') {
                    $image2Linking = 'url';
                    $linkedID2 = $ads->url2;
                } elseif ($ads->pro_id2 != '') {
                    $image2Linking = 'product';
                    $linkedID3 = $ads->pro_id2;
                }

                if ($ads->cat_id3 != '') {
                    $image3Linking = 'category';
                    $linkedID3 = $ads->cat_id3;
                } elseif ($ads->url3 != '') {
                    $image3Linking = 'url';
                    $linkedID3 = $ads->url3;
                } elseif ($ads->pro_id3 != '') {
                    $image3Linking = 'product';
                    $linkedID3 = $ads->pro_id3;
                }

                $images[] = array(
                    'size' => 'M',
                    'linkedto' => $image1Linking,
                    'linkedid' => $linkedID1,
                    'image' => $ads->image1,
                );

                $images[] = array(
                    'size' => 'M',
                    'linkedto' => $image2Linking,
                    'linkedid' => $linkedID2,
                    'image' => $ads->image2,
                );

                $images[] = array(
                    'size' => 'M',
                    'linkedto' => $image3Linking,
                    'linkedid' => $linkedID3,
                    'image' => $ads->image3,
                );

                $item[] = array(
                    'layout' => 'advertisements',
                    'name' => $ads->layout,
                    'enable' => true,
                    'path' => url('/images/adv/'),
                    'items' => $images,
                );
            }
        }

        $twong = Adv::where('position', '=', 'afterfeaturedproduct')->where('layout', 'Two non equal image layout')->where('status', '=', 1)->get();

        if (!empty($twong)) {
            foreach ($twong as $key => $ads) {

                $images = array();

                if ($ads->cat_id1 != '') {
                    $image1Linking = 'category';
                    $linkedID1 = $ads->cat_id1;
                } elseif ($ads->url1 != '') {
                    $image1Linking = 'url';
                    $linkedID1 = $ads->url1;
                } elseif ($ads->pro_id1 != '') {
                    $image1Linking = 'product';
                    $linkedID1 = $ads->pro_id1;
                }

                if ($ads->cat_id2 != '') {
                    $image2Linking = 'category';
                    $linkedID2 = $ads->cat_id2;
                } elseif ($ads->url2 != '') {
                    $image2Linking = 'url';
                    $linkedID2 = $ads->url2;
                } elseif ($ads->pro_id2 != '') {
                    $image2Linking = 'product';
                    $linkedID3 = $ads->pro_id2;
                }

                $images[] = array(
                    'size' => 'L',
                    'linkedto' => $image1Linking,
                    'linkedid' => $linkedID1,
                    'image' => $ads->image1,
                );

                $images[] = array(
                    'size' => 'S',
                    'linkedto' => $image2Linking,
                    'linkedid' => $linkedID2,
                    'image' => $ads->image2,
                );

                $item[] = array(
                    'layout' => 'advertisements',
                    'name' => $ads->layout,
                    'enable' => true,
                    'path' => url('/images/adv/'),
                    'items' => $images,
                );
            }
        }

        $twoeq = Adv::where('position', '=', 'afterfeaturedproduct')->where('layout', 'Two equal image layout')->where('status', '=', 1)->get();

        if (!empty($twoeq)) {
            foreach ($twoeq as $key => $ads) {

                $images = array();

                if ($ads->cat_id1 != '') {
                    $image1Linking = 'category';
                    $linkedID1 = $ads->cat_id1;
                } elseif ($ads->url1 != '') {
                    $image1Linking = 'url';
                    $linkedID1 = $ads->url1;
                } elseif ($ads->pro_id1 != '') {
                    $image1Linking = 'product';
                    $linkedID1 = $ads->pro_id1;
                }

                if ($ads->cat_id2 != '') {
                    $image2Linking = 'category';
                    $linkedID2 = $ads->cat_id2;
                } elseif ($ads->url2 != '') {
                    $image2Linking = 'url';
                    $linkedID2 = $ads->url2;
                } elseif ($ads->pro_id2 != '') {
                    $image2Linking = 'product';
                    $linkedID3 = $ads->pro_id2;
                }

                $images[] = array(
                    'size' => 'M',
                    'linkedto' => $image1Linking,
                    'linkedid' => $linkedID1,
                    'image' => $ads->image1,
                );

                $images[] = array(
                    'size' => 'M',
                    'linkedto' => $image2Linking,
                    'linkedid' => $linkedID2,
                    'image' => $ads->image2,
                );

                $item[] = array(
                    'layout' => 'advertisements',
                    'name' => $ads->layout,
                    'enable' => true,
                    'path' => url('/images/adv/'),
                    'items' => $images,
                );
            }
        }

        $singleimg = Adv::where('position', '=', 'afterfeaturedproduct')->where('layout', 'Single image layout')->where('status', '=', 1)->get();

        if (!empty($singleimg)) {
            foreach ($singleimg as $key => $ads) {

                $images = array();

                if ($ads->cat_id1 != '') {
                    $image1Linking = 'category';
                    $linkedID1 = $ads->cat_id1;
                } elseif ($ads->url1 != '') {
                    $image1Linking = 'url';
                    $linkedID1 = $ads->url1;
                } elseif ($ads->pro_id1 != '') {
                    $image1Linking = 'product';
                    $linkedID1 = $ads->pro_id1;
                }

                $images[] = array(
                    'size' => 'XL',
                    'linkedto' => $image1Linking,
                    'linkedid' => $linkedID1,
                    'image' => $ads->image1,
                );

                $item[] = array(
                    'layout' => 'advertisements',
                    'name' => $ads->layout,
                    'enable' => true,
                    'path' => url('/images/adv/'),
                    'items' => $images,
                );
            }
        }
        /** End */

        /** Sidebar Categories */
        $response = $this->sidebarcategories($content);

        $item[] = array(
            'name' => 'categories',
            'layout' => 'categoryblock',
            'enable' => true,
            'path' => url('/images/categories/'),
            'items' => $response,
        );
        /** End */

        /** Hotdeal products */
        $response = $this->hotdeals($content);
        /** End */

        $item[] = array(
            'layout' => 'productblock',
            'name' => 'hotdeals',
            'enable' => true,
            'path' => url('/variantimages/thumbnails/'),
            'items' => $response,
        );

        /** Hotdeal products */
        $response = $this->specialoffer($content);
        /** End */

        $item[] = array(
            'layout' => 'productblock',
            'name' => 'specialoffers',
            'enable' => true,
            'path' => url('/variantimages/thumbnails/'),
            'items' => $response,
        );

        /** Testimonial Products */
        $response = $this->testimonials($content);

        $item[] = array(
            'scrolltype' => 'vertical',
            'name' => 'testimonial',
            'layout' => 'testimonwials',
            'enable' => true,
            'path' => url('/images/testimonial/'),
            'items' => $response,
        );
        /** End */

        return response()->json($item);
          

    }

    public function sidebarcategories($content)
    {

        $categories = Category::orderBy('position', 'ASC')->select('title as title', 'id', 'image', 'icon')->get();

        foreach ($categories as $key => $cat) {
            $content[] = array(
                'id' => $cat->id,
                'title' => $cat->getTranslations('title'),
                'icon' => $cat->icon,
                'image' => $cat->image,
                'url' => url('/api/category/' . $cat->id),
            );
        }

        return $content;
    }

    public function appSettings()
    {

        $settings = Genral::first();

        if (isset($settings)) {
            return response()->json(['logo' => $settings->logo, 'logopath' => url('/images/genral/')]);
        }
    }

    public function slider($content)
    {

        $sliders = Slider::where('status', '=', '1')->get();

        foreach ($sliders as $key => $slider) {

            $type = '';

            if ($slider->link_by == 'cat') {

                $type = 'category';

            } elseif ($slider->link_by == 'sub') {
                $type = 'subcategory';
            } elseif ($slider->link_by == 'url') {
                $type = 'subcategory';
            } else {
                $type = 'None';
            }

            if ($slider->link_by == 'cat') {

                $id = $slider->category_id;

            } elseif ($slider->link_by == 'sub') {
                $id = $slider->child;
            } elseif ($slider->link_by == 'url') {
                $id = $slider->url;
            }

            $content[] = array(

                'image' => $slider->image,
                'linkedTo' => $type,
                'linked_id' => $id,
                'topheading' => $slider->getTranslations('topheading'),
                'headingtextcolor' => $slider->headingtextcolor,
                'heading' => $slider->getTranslations('heading'),
                'subheadingcolor' => $slider->subheadingcolor,
                'buttonname' => $slider->getTranslations('buttonname'),
                'btntextcolor' => $slider->btntextcolor,
                'btnbgcolor' => $slider->btnbgcolor,
                'moredescription' => $slider->moredesc != null ? $slider->moredesc : 'Not found',
                'descriptionTextColor' => $slider->moredesccolor,
                'status' => $slider->status,
            );

        }

        return $content;
    }

    public function recentProducts($content)
    {

        $products = Product::orderBy('id', 'DESC')->take(20)->get();

        foreach ($products as $product) {
            if ($product->subvariants) {

                $attributeName = array();

                foreach ($product->subvariants as $orivar) {

                    $variant = $this->getVariant($orivar);

                    $variant = $variant->getData();

                    array_push($attributeName, $variant->attrName);

                    $attributeName = array_unique($attributeName);

                    $mainprice = $this->getprice($product, $orivar);

                    $price = $mainprice->getData();

                    $rating = $this->getproductrating($product);

                    $content[] = array(
                        'productId' => $product->id,
                        'variantid' => $orivar->id,
                        'productname' => $product->getTranslations('name'),
                        'variantname' => $variant->value,
                        'description' => $product->getTranslations('des'),
                        'price' => $price,
                        'attributes' => $attributeName,
                        'rating' => $rating,
                        'thumbnail' => $orivar->variantimages->main_image,
                    );

                }
            }
        }

        return $content;
    }

    public function topcategories($content)
    {

        $topcats = CategorySlider::first();

        if ($topcats) {

            foreach ($topcats->category_ids as $categoryid) {

                $category = Category::where('id', $categoryid)->where('status', '1')->first();

                if ($category) {

                    $content[] = array(
                        'id' => $category->id,
                        'name' => $category->getTranslations('title'),
                        'description' => $category->getTranslations('description'),
                        'image' => $category->image,
                        'icon' => $category->icon,
                        'url' => url('/api/category/' . $category->id),
                    );

                }

            }

        }

        return $content;

    }

    public function categories(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'secret' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['Secret Key is required']);
        }

        $key = DB::table('api_keys')->where('secret_key', '=', $request->secret)->first();

        if (!$key) {
            return response()->json(['Invalid Secret Key !']);
        }

        $categories = Category::orderBy('position', 'ASC')->get();
        return response()->json(['categories' => $categories]);
    }

    public function gettingBlogs($content)
    {

        $blogs = Blog::where('status', '1')->get();

        foreach ($blogs as $blog) {

            $content[] = array([
                'title' => $blog->getTranslations('heading'),
                'des' => $blog->getTranslations('des'),
                'author' => $blog->getTranslations('user'),
                'image' => $blog->image,
                'url' => url('/api//blog/post/' . $blog->slug),
            ]);

        }

        return $content;
    }

    public function featuredProducts($content)
    {

        $featuredproducts = Product::where('featured', '=', '1')->orderBy('id', 'DESC')->take(20)->get();

        foreach ($featuredproducts as $product) {
            if ($product->subvariants) {

                $attributeName = array();

                foreach ($product->subvariants as $orivar) {

                    $variant = $this->getVariant($orivar);

                    $variant = $variant->getData();

                    array_push($attributeName, $variant->attrName);

                    $attributeName = array_unique($attributeName);

                    $mainprice = $this->getprice($product, $orivar);

                    $price = $mainprice->getData();

                    $rating = $this->getproductrating($product);

                    $content[] = array(
                        'productId' => $product->id,
                        'variantid' => $orivar->id,
                        'productname' => $product->getTranslations('name'),
                        'variantname' => $variant->value,
                        'description' => $product->getTranslations('des'),
                        'price' => $price,
                        'attributes' => $attributeName,
                        'rating' => $rating,
                        'thumbnail' => $orivar->variantimages->main_image,
                    );

                }
            }
        }

        return $content;

    }

    public function testimonials($content)
    {

        $testimonials = Testimonial::orderBy('id', 'DESC')->where('status', '1')->get();

        foreach ($testimonials as $value) {

            $content[] = array(
                'name' => $value->getTranslations('name'),
                'des' => $value->getTranslations('des'),
                'designation' => $value->post,
                'rating' => $value->rating,
                'profilepicture' => $value->image,
            );

        }

        return $content;
    }

    public function subcategories(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'secret' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['Secret Key is required']);
        }

        $key = DB::table('api_keys')->where('secret_key', '=', $request->secret)->first();

        if (!$key) {
            return response()->json(['Invalid Secret Key !']);
        }

        $categories = Subcategory::orderBy('position', 'ASC')->get();
        return response()->json(['categories' => $categories]);
    }

    public function childcategories(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'secret' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['Secret Key is required']);
        }

        $key = DB::table('api_keys')->where('secret_key', '=', $request->secret)->first();

        if (!$key) {
            return response()->json(['Invalid Secret Key !']);
        }

        $categories = Grandcategory::orderBy('position', 'ASC')->get();
        return response()->json(['categories' => $categories]);
    }

    public function getcategoryproduct(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'secret' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['Secret Key is required']);
        }

        $key = DB::table('api_keys')->where('secret_key', '=', $request->secret)->first();

        if (!$key) {
            return response()->json(['Invalid Secret Key !']);
        }

        $cat = Category::find($id);

        if (!$cat) {
            return response()->json(['Category not found !']);
        }

        if ($cat->status != 1) {
            return response()->json(['Category is not active !']);
        }

        $pros = $cat->products;

        $result = array();

        $attributeName = array();

        foreach ($pros as $pro) {

            if ($pro->subvariants) {

                foreach ($pro->subvariants as $orivar) {

                    $variant = $this->getVariant($orivar);

                    $variant = $variant->getData();

                    array_push($attributeName, $variant->attrName);

                    $attributeName = array_unique($attributeName);

                    $mainprice = $this->getprice($pro, $orivar);

                    $price = $mainprice->getData();

                    $rating = $this->getproductrating($pro);

                    $image['image1'] = $orivar->variantimages->image1;
                    $image['image2'] = $orivar->variantimages->image2;
                    $image['image3'] = $orivar->variantimages->image3;
                    $image['image4'] = $orivar->variantimages->image4;
                    $image['image5'] = $orivar->variantimages->image5;
                    $image['image6'] = $orivar->variantimages->image6;
                    $image['thumbnail'] = $orivar->variantimages->main_image;

                    $image = array_filter($image);

                    $result[] = array(
                        'productname' => $pro->getTranslations('name'),
                        'variantname' => $variant->value,
                        'description' => $pro->getTranslations('des'),
                        'price' => $price,
                        'attributes' => $attributeName,
                        'rating' => $rating,
                        'imagepath' => url('variantimages'),
                        'thumbpath' => url('variantimages/thumbnails/'),
                        'images' => $image,
                    );

                }

            }

        }

        if (empty($result)) {
            return response()->json('No Products Found in this category !');
        }

        return response()->json($result);

    }

    public function getsubcategoryproduct(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'secret' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['Secret Key is required']);
        }

        $key = DB::table('api_keys')->where('secret_key', '=', $request->secret)->first();

        if (!$key) {
            return response()->json(['Invalid Secret Key !']);
        }

        $subcat = Subcategory::find($id);

        if (!$subcat) {
            return response()->json(['Category not found !']);
        }

        if ($subcat->status != 1) {
            return response()->json(['Category is not active !']);
        }

        $pros = $subcat->products;

        $result = array();

        $attributeName = array();

        foreach ($pros as $pro) {

            if ($pro->subvariants) {

                foreach ($pro->subvariants as $orivar) {

                    $variant = $this->getVariant($orivar);

                    $variant = $variant->getData();

                    array_push($attributeName, $variant->attrName);

                    $attributeName = array_unique($attributeName);

                    $mainprice = $this->getprice($pro, $orivar);

                    $price = $mainprice->getData();

                    $rating = $this->getproductrating($pro);

                    $image['image1'] = $orivar->variantimages->image1;
                    $image['image2'] = $orivar->variantimages->image2;
                    $image['image3'] = $orivar->variantimages->image3;
                    $image['image4'] = $orivar->variantimages->image4;
                    $image['image5'] = $orivar->variantimages->image5;
                    $image['image6'] = $orivar->variantimages->image6;
                    $image['thumbnail'] = $orivar->variantimages->main_image;

                    $image = array_filter($image);

                    $result[] = array(
                        'productname' => $pro->getTranslations('name'),
                        'variantname' => $variant->value,
                        'description' => $pro->getTranslations('des'),
                        'price' => $price,
                        'attributes' => $attributeName,
                        'rating' => $rating,
                        'imagepath' => url('variantimages'),
                        'thumbpath' => url('variantimages/thumbnails/'),
                        'images' => $image,
                    );

                }

            }

        }

        if (empty($result)) {
            return response()->json('No Products Found in this category !');
        }

        return response()->json($result);

    }

    public function getchildcategoryproduct(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'secret' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['Secret Key is required']);
        }

        $key = DB::table('api_keys')->where('secret_key', '=', $request->secret)->first();

        if (!$key) {
            return response()->json(['Invalid Secret Key !']);
        }

        $childcat = Grandcategory::find($id);

        if (!$childcat) {
            return response()->json(['Childcategory not found !']);
        }

        if ($childcat->status != 1) {
            return response()->json(['Childcategory is not active !']);
        }

        $pros = $childcat->products;

        $result = array();

        $attributeName = array();

        foreach ($pros as $pro) {

            if ($pro->subvariants) {

                foreach ($pro->subvariants as $orivar) {

                    $variant = $this->getVariant($orivar);

                    $variant = $variant->getData();

                    array_push($attributeName, $variant->attrName);

                    $attributeName = array_unique($attributeName);

                    $mainprice = $this->getprice($pro, $orivar);

                    $price = $mainprice->getData();

                    $rating = $this->getproductrating($pro);

                    $image['image1'] = $orivar->variantimages->image1;
                    $image['image2'] = $orivar->variantimages->image2;
                    $image['image3'] = $orivar->variantimages->image3;
                    $image['image4'] = $orivar->variantimages->image4;
                    $image['image5'] = $orivar->variantimages->image5;
                    $image['image6'] = $orivar->variantimages->image6;
                    $image['thumbnail'] = $orivar->variantimages->main_image;

                    $image = array_filter($image);

                    $result[] = array(
                        'productname' => $pro->getTranslations('name'),
                        'variantname' => $variant->value,
                        'description' => $pro->getTranslations('des'),
                        'price' => $price,
                        'attributes' => $attributeName,
                        'rating' => $rating,
                        'imagepath' => url('variantimages'),
                        'thumbpath' => url('variantimages/thumbnails/'),
                        'images' => $image,
                    );

                }

            }

        }

        if (empty($result)) {
            return response()->json('No Products Found in this category !');
        }

        return response()->json($result);
    }

    public function hotdeals($content)
    {
        $hotdeals = Hotdeal::where('status', '=', '1')->get();

        $attributeName = array();

        foreach ($hotdeals as $deal) {

            if ($deal->pro->subvariants) {

                foreach ($deal->pro->subvariants as $key => $orivar) {

                    $variant = $this->getVariant($orivar);

                    $variant = $variant->getData();

                    array_push($attributeName, $variant->attrName);

                    $attributeName = array_unique($attributeName);

                    $mainprice = $this->getprice($deal->pro, $orivar);

                    $price = $mainprice->getData();

                    $rating = $this->getproductrating($deal->pro);

                    $content[] = array(
                        'start_date' => $deal->start,
                        'end_date' => $deal->end,
                        'variantid' => $orivar->id,
                        'productid' => $deal->pro->id,
                        'productname' => $deal->pro->getTranslations('name'),
                        'variantname' => $variant->value,
                        'price' => $price,
                        'attributes' => $attributeName,
                        'rating' => $rating,
                        'thumbnail' => $orivar->variantimages->main_image,
                    );

                }

            }

        }

        return $content;
    }

    public function specialoffer($content)
    {

        $attributeName = array();

        $specialOffers = SpecialOffer::where('status', '=', '1')->get();

        if (empty($specialOffers)) {
            return response()->json('No Hotdeals created !');
        }

        foreach ($specialOffers as $sp) {

            if (isset($sp->pro)) {
                if (isset($sp->pro->subvariants)) {

                    foreach ($sp->pro->subvariants as $key => $orivar) {

                        $variant = $this->getVariant($orivar);

                        $variant = $variant->getData();

                        array_push($attributeName, $variant->attrName);

                        $attributeName = array_unique($attributeName);

                        $mainprice = $this->getprice($sp->pro, $orivar);

                        $price = $mainprice->getData();

                        $rating = $this->getproductrating($sp->pro);

                        $content[] = array(
                            'productname' => $sp->pro->getTranslations('name'),
                            'variantname' => $variant->value,
                            'price' => $price,
                            'attributes' => $attributeName,
                            'rating' => $rating,
                            'thumbnail' => $orivar->variantimages->main_image,
                        );

                    }

                }
            }

        }

        return $content;
    }

    public function brands(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'secret' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['Secret Key is required']);
        }

        $key = DB::table('api_keys')->where('secret_key', '=', $request->secret)->first();

        if (!$key) {
            return response()->json(['Invalid Secret Key !']);
        }

        $brand = Brand::where('status', '=', '1')->get();
        return response()->json($brand);
    }

    public function page(Request $request, $slug)
    {
        $validator = Validator::make($request->all(), [
            'secret' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['Secret Key is required']);
        }

        $key = DB::table('api_keys')->where('secret_key', '=', $request->secret)->first();

        if (!$key) {
            return response()->json(['Invalid Secret Key !']);
        }

        $page = Page::where('slug', '=', $slug)->first();
        return response()->json($page);

    }

    public function menus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'secret' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['Secret Key is required']);
        }

        $key = DB::table('api_keys')->where('secret_key', '=', $request->secret)->first();

        if (!$key) {
            return response()->json(['Invalid Secret Key !']);
        }

        $topmenu = Menu::orderBy('position', 'ASC')->get();

        return response()->json($topmenu);
    }

    public function footermenus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'secret' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['Secret Key is required']);
        }

        $key = DB::table('api_keys')->where('secret_key', '=', $request->secret)->first();

        if (!$key) {
            return response()->json(['Invalid Secret Key !']);
        }

        $footermenus = FooterMenu::get();

        return response()->json($footermenus = FooterMenu::get());
    }

    public function userprofile(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'secret' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['Secret Key is required']);
        }

        $key = DB::table('api_keys')->where('secret_key', '=', $request->secret)->first();

        if (!$key) {
            return response()->json(['Invalid Secret Key !']);
        }

        if (!Auth::check()) {
            return response()->json("You're not logged in !");
        } else {
            $user = Auth::user();
            return response()->json($user);
        }

    }

    public function mywallet(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'secret' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['Secret Key is required']);
        }

        $key = DB::table('api_keys')->where('secret_key', '=', $request->secret)->first();

        if (!$key) {
            return response()->json(['Invalid Secret Key !']);
        }

        if (!Auth::check()) {
            return response()->json("You're not logged in !");
        }

        $wallet = UserWallet::firstWhere('user_id', '=', Auth::user()->id);
        $wallethistory = $wallet->wallethistory;
        return response()->json(['wallet' => $wallet, 'wallethistory' => $wallethistory]);
    }

    public function getuseraddress(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'secret' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['Secret Key is required']);
        }

        $key = DB::table('api_keys')->where('secret_key', '=', $request->secret)->first();

        if (!$key) {
            return response()->json(['Invalid Secret Key !']);
        }

        if (!Auth::check()) {
            return response()->json("You're not logged in !");
        }

        $address = Auth::user()->addresses;
        return response()->json($address);
    }

    public function getuserbanks(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'secret' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['Secret Key is required']);
        }

        $key = DB::table('api_keys')->where('secret_key', '=', $request->secret)->first();

        if (!$key) {
            return response()->json(['Invalid Secret Key !']);
        }

        if (!Auth::check()) {
            return response()->json("You're not logged in !");
        }

        $userbanklist = Auth::user()->banks;
        return response()->json($userbanklist);
    }

    public function faqs(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'secret' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['Secret Key is required']);
        }

        $key = DB::table('api_keys')->where('secret_key', '=', $request->secret)->first();

        if (!$key) {
            return response()->json(['Invalid Secret Key !']);
        }

        $faqs = Faq::all();

        return response()->json($faqs);
    }

    public function listallblog(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'secret' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['Secret Key is required']);
        }

        $key = DB::table('api_keys')->where('secret_key', '=', $request->secret)->first();

        if (!$key) {
            return response()->json(['Invalid Secret Key !']);
        }

        $blogs = Blog::orderBy('id', 'DESC')->get();
        return response()->json($blogs);
    }

    public function blogdetail(Request $request, $slug)
    {

        $validator = Validator::make($request->all(), [
            'secret' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['Secret Key is required']);
        }

        $key = DB::table('api_keys')->where('secret_key', '=', $request->secret)->first();

        if (!$key) {
            return response()->json(['Invalid Secret Key !']);
        }

        $blog = Blog::firstWhere('slug', '=', $slug);

        if (!isset($blog)) {
            return response()->json('404 Blog post not found !');
        }

        return response()->json($blog);
    }

    public function myNotifications(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'secret' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['Secret Key is required']);
        }

        $key = DB::table('api_keys')->where('secret_key', '=', $request->secret)->first();

        if (!$key) {
            return response()->json(['Invalid Secret Key !']);
        }

        if (!Auth::check()) {
            return response()->json("You're not logged in !");
        }

        $notifications = auth()->user()->unreadNotifications->where('n_type', '!=', 'order_v');

        $notificationsCount = auth()->user()->unreadNotifications->where('n_type', '!=', 'order_v')->count();

        return response()->json(['notifications' => $notifications, 'count' => $notificationsCount]);
    }

    public function getprice($pro, $orivar)
    {

        $convert_price = 0.00;
        $show_price = 0.00;

        $commision_setting = CommissionSetting::first();

        if ($commision_setting->type == "flat") {

            $commission_amount = $commision_setting->rate;

            if ($commision_setting->p_type == 'f') {

                if ($pro->tax_r != '') {

                    $cit = $commission_amount * $pro->tax_r / 100;
                    $totalprice = $pro->vender_price + $orivar->price + $commission_amount + $cit;
                    $totalsaleprice = $pro->vender_offer_price + $cit + $orivar->price +
                        $commission_amount;

                    if ($pro->vender_offer_price == null) {
                        $show_price = $totalprice;
                    } else {
                        $totalsaleprice;
                        $convert_price = $totalsaleprice == '' ? $totalprice : $totalsaleprice;
                        $show_price = $totalprice;
                    }

                } else {
                    $totalprice = $pro->vender_price + $orivar->price + $commission_amount;
                    $totalsaleprice = $pro->vender_offer_price + $orivar->price + $commission_amount;

                    if ($pro->vender_offer_price == null) {
                        $show_price = $totalprice;
                    } else {
                        $totalsaleprice;
                        $convert_price = $totalsaleprice == '' ? $totalprice : $totalsaleprice;
                        $show_price = $totalprice;
                    }

                }

            } else {

                $totalprice = ($pro->vender_price + $orivar->price) * $commission_amount;

                $totalsaleprice = ($pro->vender_offer_price + $orivar->price) * $commission_amount;

                $buyerprice = ($pro->vender_price + $orivar->price) + ($totalprice / 100);

                $buyersaleprice = ($pro->vender_offer_price + $orivar->price) + ($totalsaleprice / 100);

                if ($pro->vender_offer_price == null) {
                    $show_price = round($buyerprice, 2);
                } else {
                    round($buyersaleprice, 2);

                    $convert_price = $buyersaleprice == '' ? $buyerprice : $buyersaleprice;
                    $show_price = $buyerprice;
                }

            }
        } else {

            $comm = Commission::where('category_id', $pro->category_id)->first();
            if (isset($comm)) {
                if ($comm->type == 'f') {

                    if ($pro->tax_r != '') {

                        $cit = $comm->rate * $pro['tax_r'] / 100;

                        $price = $pro->vender_price + $comm->rate + $orivar->price + $cit;

                        if ($pro->vender_offer_price != null) {
                            $offer = $pro->vender_offer_price + $comm->rate + $orivar->price + $cit;
                        } else {
                            $offer = $pro->vender_offer_price;
                        }

                        if ($pro->vender_offer_price == null) {
                            $show_price = $price;
                        } else {

                            $convert_price = $offer;
                            $show_price = $price;
                        }

                    } else {

                        $price = $pro->vender_price + $comm->rate + $orivar->price;

                        if ($pro->vender_offer_price != null) {
                            $offer = $pro->vender_offer_price + $comm->rate + $orivar->price;
                        } else {
                            $offer = $pro->vender_offer_price;
                        }

                        if ($pro->vender_offer_price == 0 || $pro->vender_offer_price == null) {
                            $show_price = $price;
                        } else {

                            $convert_price = $offer;
                            $show_price = $price;
                        }

                    }

                } else {

                    $commission_amount = $comm->rate;

                    $totalprice = ($pro->vender_price + $orivar->price) * $commission_amount;

                    $totalsaleprice = ($pro->vender_offer_price + $orivar->price) * $commission_amount;

                    $buyerprice = ($pro->vender_price + $orivar->price) + ($totalprice / 100);

                    $buyersaleprice = ($pro->vender_offer_price + $orivar->price) + ($totalsaleprice / 100);

                    if ($pro->vender_offer_price == null) {
                        $show_price = round($buyerprice, 2);
                    } else {
                        $convert_price = round($buyersaleprice, 2);

                        $convert_price = $buyersaleprice == '' ? $buyerprice : $buyersaleprice;
                        $show_price = round($buyerprice, 2);
                    }

                }
            } else {
                $commission_amount = 0;

                $totalprice = ($pro->vender_price + $orivar->price) * $commission_amount;

                $totalsaleprice = ($pro->vender_offer_price + $orivar->price) * $commission_amount;

                $buyerprice = ($pro->vender_price + $orivar->price) + ($totalprice / 100);

                $buyersaleprice = ($pro->vender_offer_price + $orivar->price) + ($totalsaleprice / 100);

                if ($pro->vender_offer_price == null) {
                    $show_price = round($buyerprice, 2);
                } else {
                    $convert_price = round($buyersaleprice, 2);

                    $convert_price = $buyersaleprice == '' ? $buyerprice : $buyersaleprice;
                    $show_price = round($buyerprice, 2);
                }
            }
        }

        $convert_price = sprintf("%.2f", $convert_price * $this->conversion_rate); //Offer Price
        $show_price = sprintf('%.2f', $show_price * $this->conversion_rate); // Main Price

        if ($convert_price == 0) {
            $convert_price = 'Not available';
        }

        return response()->json(['mainprice' => $show_price, 'offerprice' => $convert_price]);

    }

    public function getproductrating($pro)
    {

        $reviews = UserReview::where('pro_id', $pro->id)->where('status', '1')->get();

        if (!empty($reviews[0])) {

            $review_t = 0;
            $price_t = 0;
            $value_t = 0;
            $sub_total = 0;
            $count = UserReview::where('pro_id', $pro->id)->count();

            foreach ($reviews as $review) {
                $review_t = $review->price * 5;
                $price_t = $review->price * 5;
                $value_t = $review->value * 5;
                $sub_total = $sub_total + $review_t + $price_t + $value_t;
            }

            $count = ($count * 3) * 5;
            $rat = $sub_total / $count;
            $ratings_var = ($rat * 100) / 5;

            $overallrating = ($ratings_var / 2) / 10;

            return round($overallrating, 1);

        } else {
            return $overallrating = 'No Rating';
        }
    }

    public function getVariant($orivar)
    {
        $varcount = count($orivar->main_attr_value);
        $i = 0;
        $othervariantName = null;

        foreach ($orivar->main_attr_value as $key => $orivars) {

            $i++;

            $loopgetattrname = ProductAttributes::where('id', $key)->first()->attr_name;
            $getvarvalue = ProductValues::where('id', $orivars)->first();

            if ($i < $varcount) {
                if (strcasecmp($getvarvalue->unit_value, $getvarvalue->values) != 0 && $getvarvalue->unit_value != null) {
                    if ($getvarvalue->proattr->attr_name == "Color" || $getvarvalue->proattr->attr_name == "Colour" || $getvarvalue->proattr->attr_name == "color" || $getvarvalue->proattr->attr_name == "colour") {

                        $othervariantName = $getvarvalue->values . ',';

                    } else {
                        $othervariantName = $getvarvalue->values . $getvarvalue->unit_value . ',';
                    }
                } else {
                    $othervariantName = $getvarvalue->values;
                }

            } else {

                if (strcasecmp($getvarvalue->unit_value, $getvarvalue->values) != 0 && $getvarvalue->unit_value != null) {

                    if ($getvarvalue->proattr->attr_name == "Color" || $getvarvalue->proattr->attr_name == "Colour" || $getvarvalue->proattr->attr_name == "color" || $getvarvalue->proattr->attr_name == "colour") {

                        $othervariantName = $getvarvalue->values;

                    } else {
                        $othervariantName = $getvarvalue->values . $getvarvalue->unit_value;
                    }

                } else {
                    $othervariantName = $getvarvalue->values;
                }

            }

        }

        return response()->json(['value' => $othervariantName, 'attrName' => $loopgetattrname]);
    }

}
